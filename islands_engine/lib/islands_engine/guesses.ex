defmodule IslandsEngine.Guesses do
  alias IslandsEngine.Coordinate
  alias __MODULE__

  @type t :: %__MODULE__{
          hits: MapSet.t(Coordinate.t()),
          misses: MapSet.t(Coordinate.t())
        }
  @enforce_keys [:hits, :misses]
  defstruct [:hits, :misses]

  @spec new() :: t()

  def new(),
    do: %Guesses{
      hits: MapSet.new(),
      misses: MapSet.new()
    }
end
