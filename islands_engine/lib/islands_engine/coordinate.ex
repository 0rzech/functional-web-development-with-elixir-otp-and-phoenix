defmodule IslandsEngine.Coordinate do
  alias __MODULE__

  @type t :: %__MODULE__{row: pos_integer(), col: pos_integer()}
  @enforce_keys [:row, :col]
  defstruct [:row, :col]

  @board_range 1..10

  @spec new(pos_integer(), pos_integer()) :: {:ok, t()} | {:error, :invalid_coordinate}

  def new(row, col) when row in @board_range and col in @board_range,
    do: {:ok, %Coordinate{row: row, col: col}}

  def new(_, _), do: {:error, :invalid_coordinate}
end
